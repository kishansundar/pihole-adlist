#!/bin/bash

# Read input file
input_file="$1"
input_urls=()

while IFS= read -r line || [[ -n "$line" ]]; do
  input_urls+=("$line")
done < "$input_file"

# Generate YAML code
yaml_output=""

for url in "${input_urls[@]}"; do
  name=$(echo "$url" | awk -F/ '{print $(NF-1)}')
  yaml_output+="- url: $url\n"
  yaml_output+="  name: \"$name\"\n"
  yaml_output+="  enabled: true\n"
  yaml_output+="- id: $((++i))\n"
done

# Output YAML code
echo -e "$yaml_output"
