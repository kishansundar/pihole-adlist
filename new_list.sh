#!/bin/bash
# Adlists Update

url=https://v.firebog.net/hosts/lists.php?type=tick

# Download the list of URLs from v.firebog.net
url_list=$(curl -s $url)

# Append your list of URLs to the downloaded list
echo "https://dbl.oisd.nl" >> unordered
#echo "https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling/hosts" >> unordered
echo "https://gitlab.com/kishansundar/pihole-adlist/-/raw/main/blacklist" >> unordered
echo "https://raw.githubusercontent.com/jerryn70/GoodbyeAds/master/Hosts/GoodbyeAds.txt" >> unordered
echo "https://raw.githubusercontent.com/hagezi/dns-blocklists/main/domains/native.winoffice.txt" >> unordered
echo "https://raw.githubusercontent.com/hagezi/dns-blocklists/main/domains/native.apple.txt" >> unordered
#echo "https://raw.githubusercontent.com/badmojr/1Hosts/master/Lite/domains.txt" >> unordered


# Create a new file with all the URLs
echo "$url_list" >> all_urls
cat unordered >> all_urls
sort -u all_urls > basic

# Clean up
rm unordered all_urls
